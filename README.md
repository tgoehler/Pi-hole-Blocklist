# Pi-hole-Blocklist

My own Pi-hole-Blocklist with stuff I want to have blocked. Some ads, some trackers, some social networks, everything from Axel Springer, some spam and so on.

Not created with public use in mind, just my personal list where I catch what has not been blocked by other lists I use. As tailored to my needs, overblocking may occur from your perspective.

Feel free to use, but no complains :-)